import models.Circle;
import models.ResizableCircle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle(5);
        System.out.println(circle1);
        System.out.println("chu vi= " + circle1.getPerimeter());
        System.out.println("diện tích= " + circle1.getArea());



        ResizableCircle resizableCircle1 = new ResizableCircle(30);
        System.out.println(resizableCircle1);
        System.out.println("chu vi= " + resizableCircle1.getPerimeter());
        System.out.println("diện tích= " + resizableCircle1.getArea());

        resizableCircle1.resize(50);
        System.out.println("Thực hiện resize kích thước");
        System.out.println(resizableCircle1);
        System.out.println("chu vi= " + resizableCircle1.getPerimeter());
        System.out.println("diện tích= " + resizableCircle1.getArea());

    }
}
