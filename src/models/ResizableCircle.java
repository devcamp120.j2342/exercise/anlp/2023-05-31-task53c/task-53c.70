package models;

import Interfaces.Resizable;

public class ResizableCircle extends Circle implements Resizable{
    public ResizableCircle(double radius) {
        super(radius);
    }

    public ResizableCircle() {
    }

    @Override
    public String toString() {
        return "ResizableCircle[" + super.toString() +']';
    }

    
    
    @Override
    public void resize(int percent) {
        super.setRadius(radius *= percent/100.0);
    }
}
