package Interfaces;

public interface Resizable {
    public abstract void resize(int percent);
}
